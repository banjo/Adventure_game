#include <iostream>
#include <fstream>
#include <string.h>
#include <algorithm>

void inputLoc(vector<Locations*>& v)
{
  using namespace std;
    ifstream file( "Locations.dat" );

    string inputLine;
    string line;
    string locName;
    string shopLoc;
    string locPrice;
    string locKey;

    bool nameReady = false;
    bool shopReady = false;
    bool priceReady = false;
    bool keyReady = false;

    // Total Locations
    int lineNum = 0;

    if ( file )
    {
        while(getline(file, inputLine)){
          lineNum++;
          line = inputLine;   // Copy into line

          // If Empty Line
          if(line.empty()){
            cout << "Empty Line Here" << endl;
            continue;
          }
          // Comments
          if(strstr(line.c_str(), "//")){
            size_t pos = line.find("//");

            if(pos != string::npos && (pos != 0 && pos != 1)){
              line = line.substr(0, pos);
            }else{
              //cout << "Comment Here!" << endl;
              continue;
            }
          }
          // Separator
          if(strstr(line.c_str(), "+>")){
            nameReady = false;
            shopReady = false;
            priceReady = false;
            keyReady = false;
            //cout << "Separator!" << endl;
            continue;
          }

          // If Name
          if(strstr(line.c_str(), "N:")){
            size_t pos = line.find("N:");
            size_t sb;    // String Beginning
            size_t sn;    // String End

            if(pos != string::npos){
              sb = line.find('>');
              sn = line.find('<');

              if(sb != string::npos){
                if(sn != string::npos){
                  // Add name here substring
                  line = line.substr(sb + 1, sb - sn);
                  line.erase(remove(line.begin(), line.end(), '<'), line.end());
                  locName = line;
                  nameReady = true;
                }else{
                  cerr << "Couldn't get string end '<' on line: " << lineNum << endl;
                  break;
                }
              }else{
                cerr << "Couldn't get string start '>' on line: " << lineNum << endl;
                break;
              }
            }
          }

          // If Shop Location
          if(strstr(line.c_str(), "S:")){
            size_t pos = line.find("S:");
            size_t sb;    // String Beginning
            size_t sn;    // String End

            if(pos != string::npos){
              sb = line.find('>');
              sn = line.find('<');

              if(sb != string::npos){
                if(sn != string::npos){
                  // Add Shop Location here
                  line = line.substr(sb + 1,sb - sn);
                  line.erase(remove(line.begin(), line.end(), '<'), line.end());
                  shopLoc = line;
                  shopReady = true;
                }else{
                  cerr << "Couldn't get string end '<' on line: " << lineNum << endl;
                  break;
                }
              }else{
                cerr << "Couldn't get string start '>' on line: " << lineNum << endl;
                break;
              }
            }
          }

          // If Location Price
          if(strstr(line.c_str(), "C:")){
            size_t pos = line.find("C:");
            size_t sb;    // String Beginning
            size_t sn;    // String End

            if(pos != string::npos){
              sb = line.find('>');
              sn = line.find('<');

              if(sb != string::npos){
                if(sn != string::npos){
                  // Add name here substring
                  line = line.substr(sb + 1, sb - sn);
                  line.erase(remove(line.begin(), line.end(), '<'), line.end());
                  locPrice = line;
                  priceReady = true;
                }else{
                  cerr << "Couldn't get string end '<' on line: " << lineNum << endl;
                  break;
                }
              }else{
                cerr << "Couldn't get string start '>' on line: " << lineNum << endl;
                break;
              }
            }
          }

          // If LocKey
          if(strstr(line.c_str(), "K:")){
            size_t pos = line.find("K:");
            size_t sb;    // String Beginning
            size_t sn;    // String End

            if(pos != string::npos){
              sb = line.find('>');
              sn = line.find('<');

              if(sb != string::npos){
                if(sn != string::npos){
                  // Add Shop Location here
                  line = line.substr(sb + 1,sb - sn);
                  line.erase(remove(line.begin(), line.end(), '<'), line.end());
                  locKey = line;
                  keyReady = true;
                }else{
                  cerr << "Couldn't get string end '<' on line: " << lineNum << endl;
                  break;
                }
              }else{
                cerr << "Couldn't get string start '>' on line: " << lineNum << endl;
                break;
              }
            }
          }

          cout << line << endl;

          // Add To Locations
          if(nameReady && shopReady && priceReady && keyReady){
            // Add it here
            // locName, shopLoc, locPrice, locKey
            int lPrice = stoi(locPrice);
            bool lKey = (stoi(locKey)) ? true : false;

            Locations *l = new Locations(locName,shopLoc,lPrice,lKey);
            v.push_back(l);
          }

        } // End of while loop

        cout << "Total Locations: " << locations << endl;

        file.close();
    }
    else{
      cerr << "Couldn't Read Data From File!" << endl;
    }
}// End of main
