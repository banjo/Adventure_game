#include "Incs/quest.h"

// Statics
int Quest::totalQuestCount = 0;

// Default Constructor
Quest::Quest(){
  this->totalQuestCount++;
  this->q_state = Q_NOTSTARTED; // Not started by default
  this->isChosen = false;
}

// Second Constructor with all params
Quest::Quest(std::string n,std::string d,std::string a,int xp,int g,int lId,bool isM){
  this->totalQuestCount++;
  this->name = n;
  this->desc = d;
  this->advice = a;
  this->xp = xp;
  this->gold = g;
  this->q_state = Q_NOTSTARTED; // Not started by default so it won't show up in quest system
  this->isChosen = false;
  this->isMain = isM;
  this->locationId = lId;
}

// Sets
void Quest::setName(std::string in){
  this->name = in;
}
void Quest::setDesc(std::string in){
  this->desc = in;
}
void Quest::setAdvice(std::string in){
  this->advice = in;
}
void Quest::setXP(int in){
  this->xp = in;
}
void Quest::setGold(int in){
  this->gold = in;
}
void Quest::setChosen(bool in){
  this->isChosen = in;
}
void Quest::setMain(bool in){
  this->isMain = in;
}
void Quest::setLocationId(int in){
  this->locationId = in;
}

// Gets
std::string Quest::getName(){
  return this->name;
}
std::string Quest::getDesc(){
  return this->desc;
}
std::string Quest::getAdvice(){
  return this->advice;
}
int Quest::getXP(){
  return this->xp;
}
int Quest::getGold(){
  return this->gold;
}
int Quest::getTotalQuestCount(){
  return this->totalQuestCount;
}
QuestState Quest::getQuestState(){
  return this->q_state;
}
bool Quest::getIsChosen(){
  return this->isChosen;
}
bool Quest::getIsMain(){
  return this->isMain;
}
int Quest::getLocationId(){
  return this->locationId;
}

// Additional Methods
void Quest::questStart(){
  this->q_state = Q_RUNNING;
}
void Quest::questAbandon(){
  this->q_state = Q_ABANDONED;
}
void Quest::questFinish(){
  this->q_state = Q_FINISHED;
}
