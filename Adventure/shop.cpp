#include "Incs/characters.h"
#include "Incs/inventory.h"
#include <limits>
#include <sstream>
using namespace std;




// buying armor
void InsArmor(Character* x,Items& i,std::vector<Items>& inv)
{

        if(x->get_gold()>=i.get_Bgold())
            {
                for(std::deque<Items>::iterator l= x->wp.begin()+3; l !=x->wp.end(); l++)
                      del_prop(*x,*l);
                x->wp.erase(x->wp.begin()+3,x->wp.end());
                x->set_gold(x->get_gold()+i.get_Sgold());
                for (std::vector<Items>::iterator k = inv.begin()+18; k != inv.begin()+18+16; ++k)
                    if((*k).get_name()==i.get_name())
                    {
                        x->wp.push_back(*k);
                        //prop(*x,*k);
                    }
                cout<<"You bought "<<i.get_name()<<" armor!!^_^ \n";
                prop(*x,i);
                x->set_gold(x->get_gold()-i.get_Bgold());

            }
            else {cout<<"You dont have enough gold!\n";}
}


//shop table blocks (shopping)
void shcase(Character* x,Items& i,Base* Data,string& n,string& t)
{
     if(i.get_type()=="Armor")
    {
        InsArmor(x,i,Data->itms);
        return;
    }

    deque<Items>::iterator it;

        if(x->get_gold()>=i.get_Bgold())
            {
                char c;
                cout<<"\n Do you want to delete items?(Y/n): ";cin>>c;
                if(c=='Y'){
                    it=delItems(x,x->wp,n,t);
                    if(it==x->wp.end())
                        return;
                    x->set_gold(x->get_gold()+i.get_Sgold());
                    x->wp.insert(it,i);
                }
                else{
                    if(InsItem(x->wp,i))
                        cout<<"Done!";
                    else
                        return;

                }


                cout<<"You bought "<<i.get_name()<<" "<<i.get_type()<<" !!^_^ \n";
                prop(*x,i);
                x->set_gold(x->get_gold()-i.get_Bgold());

            }
            else {cout<<"You dont have enough gold!\n";}
}


//check shop files
bool shop_check(vector<Items>& itms,vector<Items>& shop_v,const string& loc)
{   string k;
    string t;
    Items X;
    std::vector<Items>::iterator i;
    std::vector<Items>::iterator j;

    ifstream ifs(loc.c_str());
    while(getline(ifs,k))
    {
        istringstream iss(k);
        while(iss>>k)
        {
            iss>>t;

        X.set_name(k);
        X.set_type(t);

        i = find(itms.begin(),itms.end(),X);
        j = find(itms.begin(),itms.end(),X);
        //cout<<(*i).get_name()<<(*j).get_name();

        if(i == j)
            shop_v.push_back(*i);
        }
    }
    if(shop_v.size()!=8)
        return false;
        ifs.close();

    return true;
}


void shop_table(vector<Items>& v)
{   int x=1;
    int y=2;
    string w="            ";
    string i="    ";
    system("clear");

cout<<" __________________________________________________________________________________\n"
    <<"|\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ \n"
    <<"|\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ \n"
    <<"|\\\\\\₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪ =====1====== ₪₪₪₪₪ =====2====== ₪₪₪₪₪ =====3====== ₪₪₪₪₪ =====4====== ₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪║"<<_rep(v[0].nn,w,x)<<"║₪₪₪₪₪║"<<_rep(v[1].nn,w,x)<<"║₪₪₪₪₪║"<<_rep(v[2].nn,w,x)<<"║₪₪₪₪₪║"<<_rep(v[3].nn,w,x)<<"║₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪║"<<_rep(v[0].tt,w,y)<<"║₪₪₪₪₪║"<<_rep(v[1].tt,w,y)<<"║₪₪₪₪₪║"<<_rep(v[2].tt,w,y)<<"║₪₪₪₪₪║"<<_rep(v[3].tt,w,y)<<"║₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪║ Attack:"<<rep_(v[0].aa,i)<<"║₪₪₪₪₪║ Attack:"<<rep_(v[1].aa,i)<<"║₪₪₪₪₪║ Attack:"<<rep_(v[2].aa,i)<<"║₪₪₪₪₪║ Attack:"<<rep_(v[3].aa,i)<<"║₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪║ Defenc:"<<rep_(v[0].dd,i)<<"║₪₪₪₪₪║ Defenc:"<<rep_(v[1].dd,i)<<"║₪₪₪₪₪║ Defenc:"<<rep_(v[2].dd,i)<<"║₪₪₪₪₪║ Defenc:"<<rep_(v[3].dd,i)<<"║₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪║ Abilit:"<<rep_(v[0].bb,i)<<"║₪₪₪₪₪║ Abilit:"<<rep_(v[1].bb,i)<<"║₪₪₪₪₪║ Abilit:"<<rep_(v[2].bb,i)<<"║₪₪₪₪₪║ Abilit:"<<rep_(v[3].bb,i)<<"║₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪║   Cost:"<<rep_(v[0].mm,i)<<"║₪₪₪₪₪║   Cost:"<<rep_(v[1].mm,i)<<"║₪₪₪₪₪║   Cost:"<<rep_(v[2].mm,i)<<"║₪₪₪₪₪║   Cost:"<<rep_(v[3].mm,i)<<"║₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪ ============ ₪₪₪₪₪ ============ ₪₪₪₪₪ ============ ₪₪₪₪₪ ============ ₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪❯❯❯❯❯❯❯❯ CHOOSE WHICH U WANT❮❮❮❮❮❮❮❮₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪ =====5====== ₪₪₪₪₪ =====6====== ₪₪₪₪₪ =====7====== ₪₪₪₪₪ =====8====== ₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪║"<<_rep(v[4].nn,w,x)<<"║₪₪₪₪₪║"<<_rep(v[5].nn,w,x)<<"║₪₪₪₪₪║"<<_rep(v[6].nn,w,x)<<"║₪₪₪₪₪║"<<_rep(v[7].nn,w,x)<<"║₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪║"<<_rep(v[4].tt,w,y)<<"║₪₪₪₪₪║"<<_rep(v[5].tt,w,y)<<"║₪₪₪₪₪║"<<_rep(v[6].tt,w,y)<<"║₪₪₪₪₪║"<<_rep(v[7].tt,w,y)<<"║₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪║ Attack:"<<rep_(v[4].aa,i)<<"║₪₪₪₪₪║ Attack:"<<rep_(v[5].aa,i)<<"║₪₪₪₪₪║ Attack:"<<rep_(v[6].aa,i)<<"║₪₪₪₪₪║ Attack:"<<rep_(v[7].aa,i)<<"║₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪║ Defenc:"<<rep_(v[4].dd,i)<<"║₪₪₪₪₪║ Defenc:"<<rep_(v[5].dd,i)<<"║₪₪₪₪₪║ Defenc:"<<rep_(v[6].dd,i)<<"║₪₪₪₪₪║ Defenc:"<<rep_(v[7].dd,i)<<"║₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪║ Abilit:"<<rep_(v[4].bb,i)<<"║₪₪₪₪₪║ Abilit:"<<rep_(v[5].bb,i)<<"║₪₪₪₪₪║ Abilit:"<<rep_(v[6].bb,i)<<"║₪₪₪₪₪║ Abilit:"<<rep_(v[7].bb,i)<<"║₪₪₪₪₪ \n"
    <<"|\\\\\\₪₪₪₪₪║   Cost:"<<rep_(v[4].mm,i)<<"║₪₪₪₪₪║   Cost:"<<rep_(v[5].mm,i)<<"║₪₪₪₪₪║   Cost:"<<rep_(v[6].mm,i)<<"║₪₪₪₪₪║   Cost:"<<rep_(v[7].mm,i)<<"║₪₪₪₪₪ \n"
    <<" \\\\\\₪₪₪₪₪ ============ ₪₪₪₪₪ ============ ₪₪₪₪₪ ============ ₪₪₪₪₪ ============ ₪₪₪₪₪ \n"
    <<"  \\\\₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪ \n"
    <<"   \\₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪₪ PRESS 9 TO EXIT SHOP ₪₪₪₪₪₪₪₪₪₪₪₪ \n\n";
}

void shop(Character* Char,Base* Data,const string& loc)
{

    vector<Items>v;
    if(!shop_check(Data->itms,v,loc)){
        cout<<"Shieeet Brawh!\n";
        return;
    }


    shop_table(v);
        int p=0;
        string wp;//weapon name
        string wptp;//weapon type
        //char inv;
        //bool rep=true;
    while(true)
    {

            cout<<"Enter Number: ";//buying weapon or armor
            cin>>p;
            if(p>=9 || p<1)
              return;
            shcase(Char,v[p-1],Data,wp,wptp);

    }
    system("clear");

   return;
}
