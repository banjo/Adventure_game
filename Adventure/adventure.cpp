#include "Incs/game.h"

int main()
{
    string choice;
    string name;
    Base *Data=new Base;


    std::vector<Character*> plr;
    //=========CREATE CHARACTERS================

    Human *A=new Human;
    plr.push_back(A);
    Orc *B=new Orc;
    plr.push_back(B);
    Elf *C=new Elf;
    plr.push_back(C);

    Data->chars = plr;


    //=---=-==-==CREATE ITEMS=-=-=-=-=-=-==-=


    ifstream ifs("Data/objects.dat");
    std::vector<Items>it;
    inputObjects(it);    // input objects from object.dat file
    ifs.close();
    Data->itms = it;

    //-=--=-=-= CREATE LOCATIONS=-=-=-=-=-=-=-=
    // Replace with just load function from game.h read from file dude
    vector<Locations*> loc;
    inputLoc(loc);
    Data->Loc = loc;


    ifstream locFunc("Data/LocationFuncs.dat",ios::in);

    string func_n;//function name
    for (unsigned j = 0; j < loc.size(); j++)
    {
      for (int k = 0; k < 3; k++) {
        locFunc>>func_n;
        loc[j]->func.push_back(func_n);
      }


    }
    locFunc.close();
    //-===-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=


    //**********WELCOME MESSEAGES***********
    vector<string> welcomeMsgs;	// Make it static damn it
    string tempString;
    ifstream wlcMsgs("Data/wel-msgs.dat");       // declare

    while(getline(wlcMsgs, tempString)){    // get
        welcomeMsgs.push_back(tempString);  // add
    }
    wlcMsgs.close();
    Data->msgs = welcomeMsgs;
    //**************************************


    //&&&&&&&&& MAKE STRING TOLOWER &&&&&&&&&&&
    std::transform(choice.begin(), choice.end(), choice.begin(), ::tolower);
    //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&



    int nlx;
    system("reset");
    //_+_+_+_+_+_+_+_+_+_+_+_ I N T R O D U C T I O N _+_+_+_+_+_+_+_+_+_
    cout<<"\n********************************************************\n"
            "* Hello!! My name is Ted and this is Adventure game!!! *\n"
            "********************************************************\n";
    cout<<"1.New Game\n";
    cout<<"2.Load Game\n";
    cout<<"3.Exit \n";
    cin>>nlx;
    cin.clear();

    Human *h1=new Human;
    Orc *o1=new Orc;
    Elf *e1=new Elf;
    switch (nlx)
    {
        case 1:
              system("clear");
              cout<<"Choose Your Character (Human,Orc,Elf): ";
              cin>>choice;
              cout<<"Insert Name (MAX.10 symb): ";
              cin>>name;
              //-------------------------------------------------------------------


              //**************DEFAULT INVENTORY***********
              if(choice=="human"){
                  h1->set_name(name);
                  h1->set_player(true);
                  Data->chars.push_back(h1);
                  hum_def_items(Data->chars[3], it);

              }
              else if(choice=="orc"){

                  o1->set_name(name);
                  o1->set_player(true);
                  Data->chars.push_back(o1);
                  orc_def_items(Data->chars[3],it);

              }
              else if(choice=="elf"){
                  e1->set_name(name);
                  e1->set_player(true);
                  Data->chars.push_back(e1);
                  elf_def_items(Data->chars[3],it);
              }
              else{
                  cout<<"something gone wrong! \n";
                  return 0;
              }
              MENU(Data);
              break;
        case 2:
              Load(Data);
              MENU(Data);
              break;
        default:
              break;

    }
    Save(Data);//loadidan ver aketebs
    delete Data;
    delete A;
    delete B;
    delete C;
    delete h1;
    delete o1;
    delete e1;
	return EXIT_SUCCESS;
}
