#include "Incs/battle.h"

Character* morph(vector<Character*> plrs){

	int N=10;
	int M=20;

	char v[20][30];
	int a=5;
	int b=5;

	int c=8;
	int d=10;

	int e=2;
	int f=17;

  int g=3;
  int h=2;

  Character *X=plrs[0];
	Character *Y=plrs[1];
	Character *Z=plrs[2];
	Character* enemy;

	for (int i=0; i!=N; ++i)
    	for (int j=0; j!=M; ++j){
				if(i==0 || i==N-1)
					v[i][j] = '=';
				else if(j==0 || j==M-1){
					v[i][0]='|';
					v[i][M-1]='|';
				}
				else
					v[i][j]=' ';
    	}

    srand (time(NULL));

  while(true){

    v[a][b]= 'Y';
    v[c][d]= 'H';//human
    v[e][f]= 'O';//orc
    v[g][h]= 'E';//elf

  	system("clear");
    for (int i=0; i!=N; i++){
      	for (int j=0; j!=M; j++)
      		cout << v[i][j];
      	cout<<endl;
		}

    v[a][b]= ' ';
    v[c][d]= ' ';
    v[e][f]= ' ';
    v[g][h]= ' ';

    std::this_thread::sleep_for(std::chrono::milliseconds(10));


    (a>N-3)?(a-=1): (a-=rand()%(2)-1);
    (b>M-3)?(b-=1): (b-=rand()%(2)-1);
    (c>N-3)?(c-=1): (c-=rand()%(2)-1);
    (d>M-3)?(d-=1): (d-=rand()%(2)-1);
    (e>N-3)?(e-=1): (e-=rand()%(2)-1);
    (f>M-3)?(f-=1): (f-=rand()%(2)-1);
    (g>N-3)?(g-=1): (g-=rand()%(2)-1);
    (h>M-3)?(h-=1): (h-=rand()%(2)-1);


    (a<2)?(a+=1): (a+=rand()%(2)-1);
    (b<2)?(b+=1): (b+=rand()%(2)-1);
    (c<2)?(c+=1): (c+=rand()%(2)-1);
    (d<2)?(d+=1): (d+=rand()%(2)-1);
    (e<2)?(e+=1): (e+=rand()%(2)-1);
    (f<2)?(f+=1): (f+=rand()%(2)-1);
    (g<2)?(g+=1): (g+=rand()%(2)-1);
    (h<2)?(h+=1): (h+=rand()%(2)-1);


    if(a==c && b==d){
			enemy = X;
			break;
		}

    if(a==e && b==f){
			enemy = Y;
			break;
		}
    if(a==g && b==h){
			enemy = Z;
			break;
		}

  } // End of While
		return enemy;
}


void battle(Character* char1,Character* char2)
{
    const int reset1=char2->get_health();
    const int reset2=char2->get_speed();
		const int reset3=char1->get_speed();

    while(true)
    {
      //cout<<reset1<<" "<<reset2<<" "<<reset3;


				cout<<char1->get_name()<<" VS "<<char2->get_name()<<endl<<endl;
				cout<<"Make your Command!!! \n";
				cout<<" 1. ATTACK \n";
				cout<<" 2. FULL ATTACK\n";
				cout<<" 3. DEFENCE \n";
				cout<<" 4. SURRENDER \n";

				int comm;
				cin>>comm;
				system("clear");
				switch (comm) {
					case 1:
					case 2:
						while(true)
						{

							if(attack(char1,char2)){
								char2->set_health(reset1);
								char2->set_speed(reset2);
								return;
							}

							if(comm == 1)
									break;
						}
							break;
					case 3: // funqciebshia gasaketebeli..jer mezareba :D
                if(defence(char1,char2)){
                    char1->set_speed(reset3);
                    return;
                  }
                break;
					case 4:
								cout<<"RUN YOU FOOL !!!!\n"; cin.ignore(1024,'\n');cin.get();return;
					default:
								break;
				}
				//break;
			}

			return;
}


void arena(Character* Char,vector<Character*> plrs)
{
    while(true)
    {
    system("clear");

    cout<<plrs;// print characters properties

            //morph(pvp);
            cout << "Press any key to continue...Type q to EXIT ARENA \n";
            //cin.ignore(1024,'\n');
            if(cin.get()=='q')
                return;

               if(Char->get_health()<1)//if he's dead
                {
                    cout<<"You have to recover until you battle again!\n";
                    //cin.ignore(1024,'\n');
                    cin.get();
                    return;
                }
            battle(Char,morph(plrs));
    }
}

void dead(Character* char1,Character* char2)
{


			char1->addxp(15);//give xp
			char2->addxp(10);
			char1->set_gold(char1->get_gold()+rand()%100+30);//give gold
			char2->set_health(0);
			cout<<"Enemy recoveried!\n";

			string w="          ";
			int p=(w.length() - char1->get_name().length())/2;
			cout<<endl;
			cout<<"\t   > WINNER < \n";
			cout<<"\t->############<-\n";
			cout<<"\t->#"<<_rep(char1->get_name(),w,p)<<"#<-\n";
			cout<<"\t->############<-\n\n";
			cout<<"Press enter to continue...";

			//T=false;
			cin.ignore(1024,'\n');
			cin.get();
			return;

}


bool attack(Character* p1,Character* p2)
{
	if((p1->get_speed()+p1->get_stamina()) >= (p2->get_speed()+p2->get_stamina()))//if you are faster
			{
			int hp=0;
			hp=p1->hit();//hit power

      int d=p2->get_defence()-hp;
      int h=p2->get_health()-hp;

			(p2->get_defence()>1)? ( p2->set_defence(d) ):( p2->set_health(h) );
			if(p2->get_defence() < 1)
					p2->set_defence(0);

			if(p2->get_health()<1)//if enemy is dead
			{
				dead(p1,p2);
				return true;
			}

			p2->inc_speed();//increase speed
			cout<<p2->get_name()<<"'s health: "<<p2->get_health()+p2->get_defence()<<" | \n";
			p1->sub_speed();//decrease speed
			return false;

			}
	else
			return attack(p2,p1);

}

bool defence(Character* p1,Character * p2)
{
	int hp=p2->hit()/2;
	int d=p1->get_defence()-hp;
	int h=p1->get_health()-hp;

	(p1->get_defence()>1)? ( p1->set_defence(d) ):( p1->set_health(h) );
	if(p1->get_defence() < 1)
			p1->set_defence(0);

	if(p1->get_health()<1){
			dead(p2,p1);
			return true;
		}
	p1->inc_speed();//increase speed
	p1->inc_speed();//increase speed
	cout<<p1->get_name()<<"'s health: "<<p1->get_health()+p1->get_defence()<<" | \n";
	cout<<"Blocked "<<hp<<endl<<endl;
	p2->sub_speed();
	return false;

}
