#include "Incs/characters.h"
#include <string>
#include <iomanip>
    //------------Constructors------------------

Character::Character(istream& in)
{
  in >> name >> strength >> health >> speed
     >> attack >> defence >> stamina
     >> xp >> max_xp >> lvl >> gold
     >> isplayer;
}
Orc::Orc()
{
  name="orc";
  strength=25+attack;
  speed=150+stamina;
  health=40+defence;
    xp=0;
    max_xp=100;
    gold=100;
    lvl=0;
    isplayer=false;
    update(info);
}

Human::Human()
{
  name="human";
  strength=15+attack;
  speed=130+stamina;
  health=50+defence;
    xp=0;
    max_xp=100;
    gold=10000;
    lvl=0;
    isplayer=false;
    update(info);
}

Elf::Elf()
{
  name="elf";
  strength=20+attack;
  speed=130+stamina;
  health=100+defence;
    xp=0;
    max_xp=100;
    gold=10000;
    lvl=0;
    isplayer=false;
    update(info);

}
  //-----------------------------------------



  //------------F U N C T I O N S-------------
void Character::set_name(string a){name=a;}
void Character::set_health(int b){health=b;}
void Character::set_strength(int c){strength=c;}
void Character::set_speed(int d){speed=d;}
void Character::set_xp(int e){xp=e;}
void Character::set_gold(int f){gold=f;}
void Character::set_player(bool x){isplayer=x;}
void Character::set_attack(int at){attack=at;}
void Character::set_defence(int de){defence=de;}
void Character::set_stamina(int st){stamina=st;}


string Character::get_name(){return name;}
int Character::get_health(){return health;}
int Character::get_strength(){return strength;}
int Character::get_speed(){return speed;}
int Character::get_xp(){return xp;}
int Character::get_max(){return max_xp;}
int Character::get_lvl(){return lvl;}
int Character::get_gold(){return gold;}
bool Character::get_player(){return isplayer;}
int Character::get_attack(){return attack;}
int Character::get_defence(){return defence;}
int Character::get_stamina(){return stamina;}


void Character::sub_speed(){speed-=rand()%20;}
void Character::inc_speed(){speed+=rand()%10;}
void Character::level_up()
{
  lvl++;
  set_health(rand()%((get_health()*lvl-get_health())+get_health()));
  set_strength(rand()%((get_strength()*lvl-get_strength())+get_strength()));
  set_speed(lvl/2*get_speed());

}



void Character::addxp(int xx)
  {
    xp+=xx;
    if(xp>max_xp)
    {
      xp=0;
      max_xp+=rand()%((max_xp*lvl-max_xp+30)+max_xp);
      level_up();
    }

  }

  int Character::hit()
  { //srand(time(NULL));
    int hh=rand()%(strength+attack-strength+1)+strength;
    cout<<"Damaged "<<hh<<"hp -> ";

      return hh;
}

void Character::update(string arr[])
{
  string i="    ";
  string n="          ";
  arr[0]="++++++++++++++++++++++";
  arr[1]="| Name:     "+_rep(get_name(),n,0);
  arr[2]="| Strength: "+rep_(get_strength(),i)+" +"+rep_(get_attack(),i);
  arr[3]="| Health:   "+rep_(get_health(),i)+" +"+rep_(get_defence(),i);
  arr[4]="| Speed:    "+rep_(get_speed(),i)+" +"+rep_(get_stamina(),i);
  arr[5]="| Exp:     ("+rep_(get_xp(),i)+"/"+rep_(get_max(),i)+")";
  arr[6]="| Level:    "+_rep(to_string(get_lvl()),n,0);
  arr[7]="| Gold:     "+_rep(to_string(get_gold()),n,0);
  arr[8]="++++++++++++++++++++++";

}

//print Characters' info
  ostream& operator <<(ostream& out,vector<Character*> ch)
{
  for (unsigned x = 0;x < ch.size();x++) {
    ch[x]->update(ch[x]->info);
  }
  for (unsigned i = 0; i < 9; i++) {
    for (unsigned j = 0; j < ch.size(); j++) {
          if(i==0 || i==8)
              out<<ch[j]->info[i]<<"+";
          else out<<ch[j]->info[i]<<"|";
    }
    out<<endl;
  }
  return out;
}

void Character::printfile(ostream& fout)
 {
   fout<<name<<" "<<strength<<" "<<health<<" "<<speed<<" "<<attack<<" "<<defence<<" "<<stamina<<" "
         <<xp<<" "<<max_xp<<" "<<lvl<<" "<<gold<<" "<<isplayer<<endl;
        // return fout;
 }

/*
{
  string i="    ";
 out<<"++++++++++++++++++++++"<<endl
    <<"| Name:     "<<ch->name<<endl
    <<"| Strength: "<<rep_(ch->strength,i)<<" +"<<ch->attack<<endl
    <<"| Speed:    "<<rep_(ch->speed,i)<<" +"<<ch->stamina<<endl
    <<"| Health:   "<<rep_(ch->health,i)<<" +"<<ch->defence<<endl
    <<"| Exp:      ("<<ch->xp<<"/"<<ch->max_xp<<")"<<endl
    <<"| Level:    "<<ch->get_lvl()<<endl
    <<"++++++++++++++++++++++"<<endl<<endl;
  return out;
}
*/

Character & Character::operator = (Character *x)
{
  this->name=x->name;
  this->strength=x->strength; this->attack=x->attack;
  this->health=x->health;		  this->defence=x->defence;
  this->speed=x->speed;			  this->stamina=x->stamina;
  this->xp=x->xp;
  this->max_xp=x->max_xp;
  this->lvl=x->lvl;
  this->gold=x->gold;
  this->isplayer=x->isplayer;
  this->wp=x->wp;
  return *this;
}
