#ifndef QUEST_H
#define QUEST_H

// Includes
#include <string>

enum QuestState{
  Q_NOTSTARTED,     // If the quest is not started and not found
  Q_RUNNING,        // If the quest is activated in the characters quest vector
  Q_FINISHED,       // If the quest is finished
  Q_ABANDONED       // If the quest is under abandoned state
};

class Quest{
private:
  std::string name;                   // Quest Name
  // Source Location Dasamatebelia Ar vici jer rogor                  XXXXX AKLIA XXXX
  // Destination Location Dasamatebelia Ar vici jer rogor             XXXXX AKLIA XXXXX
  // Aq sheidzleba trigger iyos romelsac ro sheasruleb mere gedzleva da finished ewereba XXXXX AKLIA XXXXX
  // Trigger Classi albat tavisi enum tipebit magalitad unda mokla vinme an moiparo rame nivti :> XXXXX AKLIA XXXXX
  // Romelsac ID eqneba da garkveul adgilebs eqnebat kide trigggerebi da mere id ebs shevadarebt da xps da golds mere miighebs
  int locationId;
  std::string desc;                   // Quest Description and hints
  std::string advice;                 // Advice for quest if available :>
  int xp;                             // Experience to gain using this quest
  int gold;                           // Gold to gain using this quest
  // Probably an item too O.o maybe here
  static int totalQuestCount;         // Total count
  QuestState q_state;                 // Current State of Quest
  bool isChosen;                      // Set if it's active for quest system to know
  bool isMain;
public:
  Quest();
  Quest(std::string,std::string,std::string,int,int,int,bool);
  ~Quest();

  // Sets
  void setName(std::string);
  void setDesc(std::string);
  void setAdvice(std::string);
  void setXP(int);
  void setGold(int);
  void setTotalQuestCount(int);
  void setFinishedQuestCount(int);
  void setQuestState(QuestState);
  void setChosen(bool);
  void setMain(bool);
  void setLocationId(int);

  // Gets
  std::string getName();
  std::string getDesc();
  std::string getAdvice();
  int getXP();
  int getGold();
  int getTotalQuestCount();
  int getFinishedQuestCount();
  QuestState getQuestState();
  bool getIsChosen();
  bool getIsMain();
  int getLocationId();

  // Additional Methods raa :D
  // If the state is not started it should not show up!
  // Player can have max of about 5 Quests at the same time running
  void questStart();      // Set the running state
  void questAbandon();    // Can't continue abandoned quests
  void questFinish();     // Finish it and quest system will figure out where to display should not delete.

};

#endif
