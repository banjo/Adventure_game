#ifndef INVENTORY_H
#define INVENTORY_H

#include "items.h"
#include "travel.h"
#include <string>
#include <sstream>

#define nn get_name()
#define tt get_type()
#define aa get_attack()
#define dd get_defence()
#define bb get_ablt()
#define mm get_Bgold()


class Human;
class Orc;
class Elf;
class Character;
class Locations;

struct Base
{
    vector<Character*> chars;
    vector<Items> itms;
    vector<Locations*> Loc;
    vector<string> msgs;

    Base(){}
};


 void prop(Character& ,Items& );//set Items's properties
 void del_prop(Character& ,Items& );//delete Items's properties

//human's default inventory
void hum_def_items(Character* ,std::vector<Items>& );

//orc's default inventory
void orc_def_items(Character* ,std::vector<Items>& );

//elf's default inventory
void elf_def_items(Character* ,std::vector<Items>& );

//inventory table
void inv_table(Character* );
void inputObjects(vector<Items>& );
void map(int,Base* );


string _rep(const string& b,string& x,int pos);

string rep_(const int& ,string&);

#endif
