#ifndef SHOP_H
#define SHOP_H

#include "characters.h"
//#include "inventory.h"
struct Base;
void shop(Character*,Base*,const string& );
void inputObjects(vector<Items>& );
void InsArmor(Character* ,Items& ,std::vector<Items>& );
void prop(Character& ,Items& );//set Items's properties
void del_prop(Character& ,Items& );//delete  Items's properties
bool shop_check(vector<Items>& ,vector<Items>& ,const string& );
void shop_table(vector<Items>& );
void shcase(Character* ,Items& ,Base* ,string& ,string& );




#endif
