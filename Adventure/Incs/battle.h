#include "characters.h"
#include <thread>
#include <chrono>
#include <ctime>
#include <cctype>

/*<<<<<<<<<<<<<<<<<<<<<<GLOBAL>>>>>>>>>>>>>>>>>>>>*/

Character* morph(vector<Character*> );

bool attack(Character* ,Character* );

bool defence(Character* ,Character * );

void dead(Character* ,Character* );

void battle(Character* ,Character* );

void arena(Character* ,vector<Character*> );
