#ifndef TRAVEL_H
#define TRAVEL_H
#include "shop.h"
#include <string.h>

/********************
0. Nemertea = home town
1. Flaming Stones
2. Wizard Tower
3. Shadow Mountain
4. Golden Forest
5. Dead Lake
6. Orc's Stronghold
7. Hjortar Hus
8. Black Fall
9. Final BossAssNigga
*********************/
extern int a;

class Locations
{
friend void shop(Base*,const string loc);//shop.cpp
friend void battle(Character&,Character&);//battle.cpp

public:
	string loc_name;
	string shop_tbl;
  int price;
	bool firstTime;
  std::vector<string> func;

	Locations();
	Locations(string ,string ,int , bool);

	~Locations();
};

void MENU(Base* );
void town(Character* ,vector<Locations*>& ,int );
void travel(Character* ,Base* );
void inputLoc(vector<Locations*>& );


#endif
