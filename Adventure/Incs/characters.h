#ifndef CHARACTERS_H
#define CHARACTERS_H

#include "inventory.h"
#include <ctime>
#include <algorithm>
#include "quest.h"

struct Base;
class Character//:public Inventory
{
protected:
	string name;
	int strength;		int attack=0;
	int health;			int stamina=0;
	int speed;	 		int defence=0;
	int xp;
	int max_xp;
	int lvl;
	int gold;
	bool isplayer;

public:
	deque<Items> wp;
	string info[9];
	vector<Quest*> quests;
	Character():wp(7){}
	Character(istream &);
	~Character(){}


  void set_name(string );
	void set_health(int); 	void set_defence(int);
	void set_strength(int); void set_attack(int);
	void set_speed(int); 		void set_stamina(int);
	void set_xp(int);
	void set_gold(int);
	void set_player(bool);

	void reset_health();
  void reset_speed();
  void sub_speed();
  void inc_speed();
  int hit();
	void level_up();
  void addxp(int);

  string get_name();
  int get_health();			 int get_defence();
  int get_strength();		 int get_attack();
  int get_speed();			 int get_stamina();
  int get_xp();
	int get_max();
  int get_lvl();
  int get_gold();
  bool get_player();
	Character & operator = (Character* );
	void update(string []);

	friend ostream& operator <<(ostream& ,vector<Character*> );
	 void printfile(ostream& );
	friend void battle(Character*,Character*);//battle.cpp
	friend void shop(Character*,Base*,const string& );//shop.cpp
	friend void prop(Character& ,Items& );
	friend void del_prop(Character& ,Items& );
};


class Orc:public virtual Character
{
public:
	Orc();
	~Orc(){}

	//print info
	friend ostream& operator <<(ostream& ,Orc& );

};

class Human:public virtual Character
{

public:
	Human();
	~Human(){}

	//print info
	friend ostream& operator <<(ostream& ,Human& );

};

class Elf:public virtual Character
{

public:
	Elf();
	~Elf(){}

	//print info
	friend ostream& operator <<(ostream& ,Elf& );

};

#endif
