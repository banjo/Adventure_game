#ifndef GAME_H
#define GAME_H

// Includes
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include "travel.h"

using std::vector;
using std::deque;
using std::string;

// Classes
/*
class Human;
class Orc;
class Elf;
class Character;
class Locations;

struct Base
{
    vector<Character*> chars;
    deque<Items> wpns;
    vector<Items> itms;
    Locations* Loc;
    vector<string> msgs;

    Base(){}
};
*/
void Save(Base *);

void Load(Base *);

/* Load functions here
namespace Game{
  namespace Load{

  }
}
*/
#endif
