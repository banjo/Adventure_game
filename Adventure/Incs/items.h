#ifndef ITEMS_H
#define ITEMS_H

// Includes
#include <iostream>
#include <vector>
#include <deque>
#include <fstream>
#include <string>

using namespace std;
/* Replaced this with using namespace std;
using std::cout;
using std::string;
using std::endl;
using std::cin;
using std::vector;
using std::ostream;
using std::ifstream;
using std::deque;
*/

class Character;

class Items
{
	friend struct Inventory;
	friend void prop(Character& ,Items& );
	friend void del_prop(Character& ,Items& );
protected:
	string name;
	string type;
	int attack;
	int defence;
	int ablt;
	int Bgold;//items price
	int Sgold;//items sell price
public:
	Items():name("empty"),type("empty"),attack(0),defence(0),ablt(0){}
	Items(ifstream& in){in>>name>>type>>attack>>defence>>ablt>>Bgold>>Sgold;}

	string get_name(){return name;}
	string get_type(){return type;}
	void set_name(string a){ name=a;}
	void set_type(string b){ type=b;}
	int get_attack(){return attack;}
	int get_defence(){return defence;}
	int get_ablt(){return ablt;}
	int get_Bgold(){return Bgold;}
	int get_Sgold(){return Sgold;}


	// Operator ==
  friend bool operator == ( const Items &n1, const Items &n2)
	{ return (n1.name == n2.name && n1.type == n2.type); }

	//operator <<
	friend ostream & operator <<(ostream& ,Items& );
	//sell items
	friend std::deque<Items>::iterator delItems(Character *,std::deque<Items>& v,string a,string b);

	//insert Items
	friend bool InsItem(std::deque<Items>& ,Items& );

	friend string _rep(string& ,string& ,int );
	friend void inv_table(Character* );
	Items & operator = (const Items&);
};

#endif
