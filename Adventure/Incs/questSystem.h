#ifndef QUESTSYSTEM_H
#define QUESTSYSTEM_H

// Includes
#include "quest.h"

#define MAX_RUNNING_QUESTS 5  // Ramdeni sheidzleba iyos ertad running shi

// Es sistema moagvarebs questebtan dakavshirebul yvelafers
// Sheamowmebs rom 5 ze meti quest ar gqondes aghebuli
// Daaxarisxebs statebis mixedvit da ise gichvenebs questebs.
// Axla romlebic gaqvs gashvebuli running max 5
// Abandoned romlebi gaqvs magram delete ara anda delete gavaketot da mere unda aghmoachinos xolme sadac miva tavidan
// ubralod sheicvleba state Q_NOTSTARTED ze da mere isev sheicvleba dayendeba ravi movifiqrot
// Rom daamtavreb da vfiqrob ro trigger is classi unda iyos romelic sxvadasxva lokaciebshi iqneba da questebs aghvricxavt
// mokled aq ar vici kide vfiqrob quest is classi mzadaa tavisi cpp failit da movifiqrot rogor unda
// prosta igive teqnikit mivyevi .h ar aris marto cpp ia mashin make it bevri ramis shecvla iqneboda sachiro :c araushavs :d
class QuestSystem(){

}

#endif
