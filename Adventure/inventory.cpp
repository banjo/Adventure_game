#include "Incs/inventory.h"
#include "Incs/characters.h"


//input items from Data/objects.dat file
void inputObjects(vector<Items>& it)
{
    ifstream ifs("Data/objects.dat");
    while(ifs)
    {
        Items in(ifs);
        it.push_back(in);

    }
    ifs.close();
}

void prop(Character& a,Items& x)//set Items's properties
{

    a.attack+=x.attack;
    a.defence+=x.defence;
    a.stamina+=x.ablt;

}

void del_prop(Character& a,Items& y)//delete Items's properties
{

  a.attack-=y.attack;
  a.defence-=y.defence;
  a.stamina-=y.ablt;


}
//human's default inventory
void hum_def_items(Character* Char,std::vector<Items>& itms)
  {
        Char->wp.pop_front();
        Char->wp.pop_front();
        Char->wp.pop_front();
        Char->wp.push_front(itms[1]);
        Char->wp.push_front(itms[4]);
        Char->wp.push_front(itms[7]);
        prop(*Char,itms[1]);
        prop(*Char,itms[4]);
        prop(*Char,itms[7]);

    }

//orc's default inventory
void orc_def_items(Character* Char,std::vector<Items>& itms)
    {

        Char->wp.pop_front();
        Char->wp.push_front(itms[7]);
        prop(*Char,itms[7]);
    }

//elf's default inventory
void elf_def_items(Character* Char,std::vector<Items>& itms)
    {
        Char->wp.pop_front();
        Char->wp.pop_front();
        Char->wp.push_front(itms[2]);
        Char->wp.push_front(itms[6]);
        prop(*Char,itms[2]);
        prop(*Char,itms[6]);

    }

//replace inventory's table blocks with strings
string _rep(const string& b,string& x,int pos)
{   string c=x;//string c="            ";
        c.erase(pos,b.size());//ese imitom rom cxili dalagebuli iyos. erti space ic da ireva yvelaperi
        c.insert(pos,b);
        return c;
}

//replace inventort's table blocks with integers
string rep_(const int& a,string& x)
{   string c=x;//string c="    ";
    double b=a;

    std::string s;
    std::stringstream out;//convert int to string
    if(a==0)
        return " -  ";
    if(a>999){
        b=b/1000;
        out << b;
        s = out.str()+'K';
    }
    else
    {
        out << b;
        s = out.str();
    }

        c.erase(0,s.length());
        c.insert(0,s);
        out.str("");
        return c;
}


//inventory table
void inv_table(Character* c)
{    int x=1;//position 1
     int y=2;//position 2
     string w="            ";
     string i="    ";
     string g="        ";
        cout<<"  ____________________________________________________________________________  \n"
             <<" /.........................................................................../| \n"
             <<"/.........................................................................../.| \n"
             <<"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$..| \n"
             <<"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$..| \n"
             <<"$$$$ Gold:"<<rep_(c->get_gold(),g)<<"$$$$$$$$$$$$$   W E A P O N S   $$$$$$$$$$$$$$$$$$$$$$$$$$..| \n"
             <<"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$..| \n"
             <<"$$$$$$$$$$$$$ ============ $$$$ ============ $$$$ ============ $$$$$$$$$$$$$..| \n"
             <<"$$$$$$$$$$$$$║"<<_rep(c->wp[0].nn,w,x)<<"║$$$$║"<<_rep(c->wp[1].nn,w,x)<<"║$$$$║"<<_rep(c->wp[2].nn,w,x)<<"║$$$$$$$$$$$$$..| \n"
             <<"$$$$$$$$$$$$$║"<<_rep(c->wp[0].tt,w,y)<<"║$$$$║"<<_rep(c->wp[1].tt,w,y)<<"║$$$$║"<<_rep(c->wp[2].tt,w,y)<<"║$$$$$$$$$$$$$..| \n"
             <<"$$$$$$$$$$$$$║ Attack:"<<rep_(c->wp[0].attack,i)<<"║$$$$║ Attack:"<<rep_(c->wp[1].attack,i)<<"║$$$$║ Attack:"<<rep_(c->wp[2].attack,i)<<"║$$$$$$$$$$$$$..| \n"
             <<"$$$$$$$$$$$$$║ Defenc:"<<rep_(c->wp[0].defence,i)<<"║$$$$║ Defenc:"<<rep_(c->wp[1].defence,i)<<"║$$$$║ Defenc:"<<rep_(c->wp[2].defence,i)<<"║$$$$$$$$$$$$$..| \n"
             <<"$$$$$$$$$$$$$║ Abilit:"<<rep_(c->wp[0].ablt,i)<<"║$$$$║ Abilit:"<<rep_(c->wp[1].ablt,i)<<"║$$$$║ Abilit:"<<rep_(c->wp[2].ablt,i)<<"║$$$$$$$$$$$$$..| \n"
             <<"$$$$$$$$$$$$$ ============ $$$$ ============ $$$$ ============ $$$$$$$$$$$$$..| \n"
             <<"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$..| \n"
             <<"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     A R M O R     $$$$$$$$$$$$$$$$$$$$$$$$$$$$..| \n"
             <<"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$..| \n"
             <<"$$$$ ============ $$$$ ============ $$$$ ============ $$$$ ============ $$$$..| \n"
             <<"$$$$║"<<_rep(c->wp[3].nn,w,x)<<"║$$$$║"<<_rep(c->wp[4].nn,w,x)<<"║$$$$║"<<_rep(c->wp[5].nn,w,x)<<"║$$$$║"<<_rep(c->wp[6].nn,w,x)<<"║$$$$..| \n"
             <<"$$$$║"<<_rep(c->wp[3].tt,w,y)<<"║$$$$║"<<_rep(c->wp[4].tt,w,y)<<"║$$$$║"<<_rep(c->wp[5].tt,w,y)<<"║$$$$║"<<_rep(c->wp[6].tt,w,y)<<"║$$$$..| \n"
             <<"$$$$║ Attack:"<<rep_(c->wp[3].attack,i)<<"║$$$$║ Attack:"<<rep_(c->wp[4].attack,i)<<"║$$$$║ Attack:"<<rep_(c->wp[5].attack,i)<<"║$$$$║ Attack:"<<rep_(c->wp[6].attack,i)<<"║$$$$..| \n"
             <<"$$$$║ Defenc:"<<rep_(c->wp[3].defence,i)<<"║$$$$║ Defenc:"<<rep_(c->wp[4].defence,i)<<"║$$$$║ Defenc:"<<rep_(c->wp[5].defence,i)<<"║$$$$║ Defenc:"<<rep_(c->wp[6].defence,i)<<"║$$$$..| \n"
             <<"$$$$║ Abilit:"<<rep_(c->wp[3].ablt,i)<<"║$$$$║ Abilit:"<<rep_(c->wp[4].ablt,i)<<"║$$$$║ Abilit:"<<rep_(c->wp[5].ablt,i)<<"║$$$$║ Abilit:"<<rep_(c->wp[6].ablt,i)<<"║$$$$..| \n"
             <<"$$$$ ============ $$$$ ============ $$$$ ============ $$$$ ============ $$$$..| \n"
             <<"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$..| \n"
             <<"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$./  \n"
             <<"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$/   \n";
}


void map(int a,Base* Data)
{

    string l[10] = "   ";
    l[a] = "You";
    char *X=new char[Data->Loc.size()];

    for (unsigned i = 0; i < Data->Loc.size(); i++) {
      if(Data->Loc[i]->firstTime)//is true
            X[i]='X';
      else X[i]='.';
    }

    /********************
    0. Nemertea = home town
    1. Flaming Stones
    2. Wizard Tower
    3. Shadow Mountain
    4. Golden Forest
    5. Dead Lake
    6. Orc's Stronghold
    7. Hjortar Hus
    8. Black Fall
    9. Final BossAssNigga
    *********************/

        cout<<".====================================================================================================.\n"
            <<"|                          ~~~~~~           .. -N-                                           .       |\n"
            <<"|......                   | "<<l[8]<<"  |       .-     |                .                  `......-         |\n"
            <<"|     :`                  | B F  |       :                  .....                  .-                |\n"
            <<"|     ..   ~~~~~~          ~~~~~~       -.                 -.                    ~~~~~~              |\n"
            <<"|     `:--| "<<l[6]<<"  | "<<X[6]<<".-.        :       :`                ~~~~~~                 | "<<l[4]<<"  |             |\n"
            <<"|      :` | O S  |    `-..```.."<<X[8]<<"....../         ....."<<X[1]<<".-| "<<l[1]<<"  |......   .      | G F  |             |\n"
            <<"|     `:   ~~~~~~         ```         :.    -...       `| F S  |              ./ ~~~~~~              |\n"
            <<"|    `:                                .- --             ~~~~~~             --"<<X[4]<<"                      |\n"
            <<"|    -`          ..                     :                  :               /                         |\n"
            <<"|    :`        :.  -.                   :                  :`             :                          |\n"
            <<"|    /         :    :                ~~~~~~~            `-..              :                 `        |\n"
            <<"|    `"<<X[7]<<"`        :   `.-`            |  "<<l[0]<<"  |           :`       `.      `-              `.-`        |\n"
            <<"|    ~~~~~~~~   :      :`       ....|  NE   |..-.        `.../-..`       /..-`         .-.           |\n"
            <<"|   |  "<<l[7]<<"   | -.      `:      :`    ~~~~~~~    -.            ...........`   `:.    .--`             |\n"
            <<"|   |        |"<<X[7]<<"-        :-`  `:`        :        `+......                      `-.:-`                |\n"
            <<"| | |  H  H  |           .:--.          :        :`      --.        --:            .....`          | |\n"
            <<"| W- ~~~~~~~~           -.               :       :          .......-  -.                ......... -E |\n"
            <<"| |                  .--             `-..`       `....`                :.-.      `..               | |\n"
            <<"|              /-----                :`               .-.              :  `......`  ..               |\n"
            <<"|             .`               .-....`--                :              .-                          ..|\n"
            <<"|           /`                 /        ........        `               /                         `- |\n"
            <<"|           |              `...`                -.                      `:              .......`..-  |\n"
            <<"|           /             `:                     .`                      -..```   ...-.        `     |\n"
            <<"|  ~~~~~~ -"<<X[5]<<".-.         ..."<<X[3]<<"                     `|"<<X[9]<<"----.                    ``.:/:                  |\n"
            <<"| | "<<l[5]<<"  |`   ':........    |               ......'    ~~~~~~~~~~                 : ..``             |\n"
            <<"| | D L  |       .:         ~~~~~         -.          |   "<<l[9]<<"    |                :  ~~~~~~~         |\n"
            <<"|  ~~~~~~          :       | "<<l[3]<<" |"<<X[3]<<"......:            |  FINAL   |                : |  "<<l[2]<<"  |        |\n"
            <<"|                  :       | S M |                    |   BOSS   |                "<<X[2]<<".|  W T  |        |\n"
            <<"|                  :        ~~~~~               |     |          |                   ~~~~~~~         |\n"
            <<"|                  :                           -S-     ~~~~~~~~~~                                    |\n"
            <<"|=============================================Legend=================================================|\n"
            <<"|  NE +> Nemertea    |   OS +> "<< ((X[6] == '.') ? "Orc's Stronghold" : "                ") << "   |  GF  +> " << ((X[4] == '.') ? "Golden Forest" : "             ") << "    |  WT +> " << ((X[2] == '.') ? "Wizard's Tower" : "              ") << " |\n"
            <<"|  HH +> " << ((X[7] == '.') ? "Hjortar Hus" : "          ") << "  |   SM +> " << ((X[3] == '.') ? "Shadow Of Mountain" : "                  ") << " |  FS  +> " << ((X[1] == '.') ? "Flaming Stones" : "              ") << "   |  DL +> " << ((X[5] == '.') ? "Dead Lake" : "         ") << "      |\n"
            <<"|  BF +> " << ((X[8] == '.') ? "Black Fall" : "          ") << "  |   X  +> BARIER             |  YOU +> Current location |                       |\n"
            <<"'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'\n";

            delete X;
}
